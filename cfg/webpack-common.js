const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');

const dir = require('./dir');

module.exports = {
  plugins: [
    new HtmlWebpackPlugin({
      inject: true,
      template: dir.entryHtml
    })
  ],
  resolve: {
    extensions: ['.js', '.jsx'],
    modules: ['node_modules'],
    alias: {
      Components: path.resolve(dir.sources, 'components'),
      Utils: path.resolve(dir.sources, 'utils')
    }
  },
  module: {
    rules: [
      {
        test: /\.(png|svg|jpg)$/,
        use: ['file-loader']
      }
    ]
  }
};
