const path = require('path');
const fs = require('fs');

const rootDir = fs.realpathSync(process.cwd());
const resolveDir = relativePath => path.resolve(rootDir, relativePath);

module.exports = {
  assets: resolveDir('src/assets'),
  build: resolveDir('build'),
  config: resolveDir('src/cfg'),
  entryJs: resolveDir('src/index.jsx'),
  entryHtml: resolveDir('src/index.html'),
  sources: resolveDir('src'),
};
